#self.destination_text_input = wx.TextCtrl(main_panel)
#row_box_1.Add(self.destination_text_input, proportion=1)
#destination_button = wx.Button(main_panel, wx.ID_ANY, label="Browse...")
#self.Bind(wx.EVT_BUTTON, self.select_destination_directory, id=destination_button.GetId())
#row_box_1.Add(destination_button)

def select_destination_directory(self, e):
    directory_dialog = wx.DirDialog(self, "Choose sync destination directory:")
    if directory_dialog.ShowModal() == wx.ID_OK:
        print "%s"%directory_dialog.GetPath()
        self.destination_text_input.SetValue(directory_dialog.GetPath())
        self.destination_text_input.ShowPosition(0)
    directory_dialog.Destroy()