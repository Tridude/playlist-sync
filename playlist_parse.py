import os
import sys
import distutils.file_util
import shutil
import codecs

from threading import Thread

import wx
from wx.lib.pubsub import pub
from wx.lib.pubsub import setupkwargs

cover_image_name_list = ["cover.jpg", "cover.png", "scan/cover.jpg", "scan/cover.png"]

class PlaylistThread(Thread):
    """
    Doc here
    """

    def __init__(self, playlist_path, destination_root, progress_range):
        Thread.__init__(self)
        self.cover_image_name_list = ["cover.jpg", "cover.png", "scan/cover.jpg", "scan/cover.png"]
        self.playlist_path = playlist_path
        self.destination_root = destination_root
        self.progress_range = progress_range

    def run(self):
        playlist_root_dir = self.get_playlist_root_dir(self.playlist_path)
        wx.CallAfter(pub.sendMessage, "sync", update_message="Reading playlist...\n")
        song_paths = self.read_playlist(self.playlist_path)
        if song_paths is not None:
            wx.CallAfter(pub.sendMessage, "sync", update_message="Found {} songs\nCopying...\n".format(len(song_paths)))
            progress_tick = self.progress_range / len(song_paths)
            songs_copied = []
            for song_path in song_paths:
                wx.CallAfter(pub.sendMessage, "sync", update_message=song_path)
                if self.copy_song_to_destination(song_path, playlist_root_dir) == 1:
                    songs_copied.append(song_path)
                else:
                    wx.CallAfter(pub.sendMessage, "sync", update_message="   Failed")
                wx.CallAfter(pub.sendMessage, "sync", update_message="\n", tick_value=progress_tick)
            if songs_copied:
                wx.CallAfter(pub.sendMessage, "sync", update_message="Writing playlist file...\n")
                try:
                    destination_playlist = open(os.path.join(self.destination_root, os.path.basename(self.playlist_path)), "w")
                    destination_playlist.write("\n".join(songs_copied).encode('utf-8'))
                    destination_playlist.write("\n")
                    destination_playlist.close()
                except Exception as e:
                    # print e
                    wx.CallAfter(pub.sendMessage, "sync", update_message="Failed to write playlist file\n")
            else:
                wx.CallAfter(pub.sendMessage, "sync", update_message="Failed to copy all songs\n")

        else:
            wx.CallAfter(pub.sendMessage, "sync", update_message="Failed to read playlist\n")

        wx.CallAfter(pub.sendMessage, "finish", finish_message="Finished syncing")

    """
    Returns a list of strings with the path to each song file in the playlist file.
    """
    def read_playlist(self, playlist_path):
        try:
            f1 = open(playlist_path, 'r')
            #f1 = codecs.open(playlist_path, encoding='utf-8')
        except Exception as e:
            print e
            return None
        playlist_root_dir = get_playlist_root_dir(playlist_path)
        song_paths = [a_line.decode("utf-8-sig").rstrip().lstrip(playlist_root_dir) for a_line in f1.readlines()]
        f1.close()
        return song_paths

    def get_playlist_root_dir(self, playlist_path):
        return os.path.dirname(os.path.realpath(playlist_path))

    def copy_song_to_destination(self, song_path, playlist_root_dir):
        song_root_dir = os.path.dirname(song_path)
        song_destination_root = os.path.join(self.destination_root, song_root_dir)
        song_full_path = os.path.join(playlist_root_dir, song_path)

        if not os.path.isdir(song_destination_root):
            os.makedirs(song_destination_root)
        try:
            distutils.file_util.copy_file(song_full_path, song_destination_root, update=1)
            #shutil.copy(os.path.join(playlist_root_dir, song_path), os.path.join(destination_root, song_path))
        except Exception as e:
            print e
            return 2
        for cover_name in cover_image_name_list:
            cover_path = os.path.join(playlist_root_dir, song_root_dir, cover_name)
            if os.path.isfile(cover_path):
                try:
                    distutils.file_util.copy_file(cover_path, song_destination_root, update=1)
                    # break
                except Exception as e:
                    print e
        return 1

    def sync_playlist(self, playlist_path, destination_root):
        playlist_root_dir = get_playlist_root_dir(playlist_path)
        
        song_paths = read_playlist(playlist_path)
        if not song_paths:
            return 10

        copy_success = []
        copy_fail = []
        for song_path in song_paths:
            result = copy_song_to_destination(song_path, playlist_root_dir, destination_root)
            if result == 1:
                copy_success.append(song_path)
            else:
                copy_fail.append(song_path)
        if not copy_success:
            print "No files copied successfully"
            return 11
        destination_playlist_path = os.path.join(destination_root, os.path.basename(playlist_path))
        try:
            destination_playlist = open(destination_playlist_path, 'w')
        except Exception as e:
            print e
            return 12
        destination_playlist.write("\n".join(copy_success).encode('utf-8'))
        destination_playlist.write("\n")
        destination_playlist.close()
        return 1

def read_playlist(playlist_path):
    try:
        f1 = open(playlist_path, 'r')
        #f1 = codecs.open(playlist_path, encoding='utf-8')
    except Exception as e:
        print e
        return None
    playlist_root_dir = get_playlist_root_dir(playlist_path)
    song_paths = [a_line.decode("utf-8-sig").rstrip().lstrip(playlist_root_dir) for a_line in f1.readlines()]
    f1.close()
    return song_paths

def get_playlist_root_dir(playlist_path):
    return os.path.dirname(os.path.realpath(playlist_path))

def copy_song_to_destination(song_path, playlist_root_dir, destination_root):
    song_root_dir = os.path.dirname(song_path)
    song_destination_root = os.path.join(destination_root, song_root_dir)
    song_full_path = os.path.join(playlist_root_dir, song_path)

    if not os.path.isdir(song_destination_root):
        os.makedirs(song_destination_root)
    try:
        distutils.file_util.copy_file(song_full_path, song_destination_root, update=1)
        #shutil.copy(os.path.join(playlist_root_dir, song_path), os.path.join(destination_root, song_path))
    except Exception as e:
        print e
        return 2
    for cover_name in cover_image_name_list:
        cover_path = os.path.join(playlist_root_dir, song_root_dir, cover_name)
        if os.path.isfile(cover_path):
            try:
                distutils.file_util.copy_file(cover_path, song_destination_root, update=1)
            except Exception as e:
                print e
                return 3
            break
    return 1

def sync_playlist(playlist_path, destination_root):
    playlist_root_dir = get_playlist_root_dir(playlist_path)
    
    song_paths = read_playlist(playlist_path)
    if not song_paths:
        return 10

    copy_success = []
    copy_fail = []
    for song_path in song_paths:
        result = copy_song_to_destination(song_path, playlist_root_dir, destination_root)
        if result == 1:
            copy_success.append(song_path)
        else:
            copy_fail.append(song_path)
    if not copy_success:
        print "No files copied successfully"
        return 11
    destination_playlist_path = os.path.join(destination_root, os.path.basename(playlist_path))
    try:
        destination_playlist = open(destination_playlist_path, 'w')
    except Exception as e:
        print e
        return 12
    destination_playlist.write("\n".join(copy_success).encode('utf-8'))
    destination_playlist.write("\n")
    destination_playlist.close()
    return 1

# arg_parser = argparse.ArgumentParser(
#     description="Script for syncing/copying playlist and songs."
# )
# arg_parser.add_argument("playlist_path", 
#     help="path to playlist including playlist file name."
# )
# arg_parser.add_argument("destination_root",
#     help="path to destination folder."
# )

# args = arg_parser.parse_args()

# sync_playlist(args.playlist_path, args.destination_root)

