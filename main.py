import os
import wx
import wx.richtext

import playlist_parse
from playlist_parse import PlaylistThread
from wx.lib.pubsub import pub
from wx.lib.pubsub import setupkwargs

class PlaylistGui(wx.Frame):

    def __init__(self, *args, **kwargs):
        super(PlaylistGui, self).__init__(*args, **kwargs)
        self.progress_range = 1000
        self.initialize_gui()
        self.Centre()
        self.Show()

    def initialize_gui(self):
        # menubar = wx.MenuBar()
        
        # file_menu = wx.Menu()
        # file_menu_exit = file_menu.Append(wx.ID_EXIT, "Exit", "Exit application")
        # self.Bind(wx.EVT_MENU, self.on_exit, file_menu_exit)
        # menubar.Append(file_menu, "&File")

        # self.SetMenuBar(menubar)

        main_panel = wx.Panel(self)

        font = wx.SystemSettings_GetFont(wx.SYS_SYSTEM_FONT)
        font.SetPointSize(9)

        vertical_box = wx.BoxSizer(wx.VERTICAL)

        row_box_1 = wx.BoxSizer(wx.HORIZONTAL)
        text_1 = wx.StaticText(main_panel, label="Destination:")
        #text_1.SetFont(font)
        row_box_1.Add(text_1)
        self.destination_picker = wx.DirPickerCtrl(main_panel,
            message="Choose destination directory:")
        row_box_1.Add(self.destination_picker, proportion=1)
        vertical_box.Add(row_box_1, flag=wx.EXPAND)


        row_box_2 = wx.BoxSizer(wx.HORIZONTAL)
        text_2 = wx.StaticText(main_panel, label="Playlist:")
        #text_2.SetFont(font)
        row_box_2.Add(text_2)
        self.playlist_picker = wx.FilePickerCtrl(main_panel,
            message="Choose playlist:")
        row_box_2.Add(self.playlist_picker, proportion=1)
        vertical_box.Add(row_box_2, flag=wx.EXPAND)

        row_box_3 = wx.BoxSizer(wx.HORIZONTAL)
        self.sync_button = wx.Button(main_panel, wx.ID_ANY, label="Sync")
        self.Bind(wx.EVT_BUTTON, self.sync_start, id=self.sync_button.GetId())
        row_box_3.Add(self.sync_button)
        vertical_box.Add(row_box_3, flag=wx.ALIGN_RIGHT)
        
        row_box_4 = wx.BoxSizer(wx.HORIZONTAL)
        self.status_text = wx.richtext.RichTextCtrl(main_panel, wx.ID_ANY,
            style=wx.TE_MULTILINE|wx.HSCROLL|wx.TE_READONLY)
        row_box_4.Add(self.status_text, proportion=1, flag=wx.EXPAND)
        vertical_box.Add(row_box_4, proportion=1, flag=wx.EXPAND)

        row_box_5 = wx.BoxSizer(wx.HORIZONTAL)
        self.progress_bar = wx.Gauge(main_panel, wx.ID_ANY, range=self.progress_range,
            style=wx.GA_HORIZONTAL)
        row_box_5.Add(self.progress_bar)
        vertical_box.Add(row_box_5, flag=wx.ALIGN_RIGHT)

        main_panel.SetSizer(vertical_box)

        pub.subscribe(self.sync_update, "sync")
        pub.subscribe(self.sync_finish, "finish")

    def on_exit(self, e):
        self.Close()

    def sync_playlist_to_destination(self, e):
        self.progress_bar.SetValue(0)
        self.status_text.Clear()


        # print self.destination_picker.GetPath()
        # print self.playlist_picker.GetPath()
        destination_root = self.destination_picker.GetPath()
        playlist_path = self.playlist_picker.GetPath()
        
        # self.status_text.AppendText(destination_root)
        # self.status_text.AppendText("\n")
        # self.status_text.AppendText(playlist_path)
        # self.status_text.AppendText("\n")

        playlist_root_dir = os.path.dirname(os.path.realpath(playlist_path))
    
        song_paths = playlist_parse.read_playlist(playlist_path)
        if not song_paths:
            return 10

        progress_tick = self.progress_range / len(song_paths)
        progress_value = 0
        copy_success = []
        copy_fail = []
        for song_path in song_paths:
            self.status_text.AppendText(song_path)
            result = playlist_parse.copy_song_to_destination(song_path, playlist_root_dir, destination_root)
            if result == 1:
                copy_success.append(song_path)
            else:
                self.status_text.AppendText("   Failed")
                copy_fail.append(song_path)
            self.status_text.AppendText("\n")
            progress_value += progress_tick
            self.progress_bar.SetValue(progress_value)
            self.Update()
        if not copy_success:
            print "No files copied successfully"
            return 11
        destination_playlist_path = os.path.join(destination_root, os.path.basename(playlist_path))
        try:
            destination_playlist = open(destination_playlist_path, 'w')
        except Exception as e:
            print e
            return 12
        destination_playlist.write("\n".join(copy_success).encode('utf-8'))
        destination_playlist.write("\n")
        destination_playlist.close()
        print "done"
        return 1

    def sync_start(self, event):
        self.progress_bar.SetValue(0)
        self.status_text.Clear()

        playlist_path = self.playlist_picker.GetPath()
        destination_root = self.destination_picker.GetPath()

        PlaylistThread(playlist_path, destination_root, self.progress_range).start()
        self.sync_button.Disable()

    def sync_update(self, update_message, tick_value=0):
        # print update_message
        self.status_text.AppendText(update_message)
        self.status_text.ShowPosition(self.status_text.GetLastPosition())
        if tick_value > 0:
            self.progress_bar.SetValue(self.progress_bar.GetValue() + tick_value)

    def sync_finish(self, finish_message=""):
        self.status_text.AppendText(finish_message)
        self.status_text.ShowPosition(self.status_text.GetLastPosition())
        self.progress_bar.SetValue(0)
        self.sync_button.Enable()


if __name__ == "__main__":
    app = wx.App()
    frame = PlaylistGui(None, title="Playlist Sync")
    app.MainLoop()
